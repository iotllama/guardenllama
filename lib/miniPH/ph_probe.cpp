#include "ph_probe.h"
#include <Arduino.h>
#include <Preferences.h>
SemaphoreHandle_t mutex_v;

PH_Probe::PH_Probe(int addr) {
  itsAddr = addr;
  cal1adc = 0;
  cal1ph = 0;
  cal2adc = 0;
  cal2ph = 0;
  calTemp = 25;
  mutex_v = xSemaphoreCreateMutex();
  Wire.begin();
}
void PH_Probe::calibrate() {
  if (loadCalibrationData() != PROBE_OK) {
    phStep = 0;
    return;
  }
  if (cal1adc == 0 || cal2adc == 0 || cal1ph < 1 || cal2ph < 1) {
    phStep = 0.0;
    return;
  }
  phStep = (cal2ph - cal1ph) / (cal2adc - cal1adc);
}

float PH_Probe::getPH(float temp) {
  calibrate();
  int adcValue = getADCValue();
  if (temp < 273)
    return phStep * (adcValue - cal1adc) + cal1ph;
  else
    return phStep * (temp + kelvin) / calTemp * (adcValue - cal1adc) + cal1ph;
}

float PH_Probe::getPHSmooth(float temp) {
  float last_pH_reading = 0.;
  float avg_pH = 0.;
  unsigned long currentMillis = millis();
  unsigned long start_probe_noise_readings_millis = currentMillis;
  unsigned long probe_noise_reading_period = 1000;  // time to take samples from the probe
  int cnt = 0;
  while (currentMillis - start_probe_noise_readings_millis < probe_noise_reading_period) {
    // take a pH reading
    float pH_reading = getPH(temp);
    avg_pH += (.7 * pH_reading + .3 * last_pH_reading);
    cnt++;
    last_pH_reading = pH_reading;
    currentMillis = millis();
  }
  avg_pH = avg_pH / cnt;
  return avg_pH;
}

int PH_Probe::getADCSmooth() {
  int lastAdc = 0;
  float avgAdc = 0;
  unsigned long currentMillis = millis();
  unsigned long start_probe_noise_readings_millis = currentMillis;
  unsigned long probe_noise_reading_period = 1000;  // time to take samples from the probe
  int cnt = 0;
  while (currentMillis - start_probe_noise_readings_millis < probe_noise_reading_period) {
    // take a pH reading
    int adcReading = getADCValue();
    avgAdc += .7 * adcReading + .3 * lastAdc;
    cnt++;
    // don't check readings until a few have come in.  This will hopefully accomodate any initial stray readings that
    // might happen
    lastAdc = adcReading;
    currentMillis = millis();
  }
  avgAdc = avgAdc / cnt;
  return avgAdc;
}

int PH_Probe::getADCValue() {
  short adc_high;
  short adc_low;
  // We'll assemble the 2 in this variable
  int adc_result;
  // Wire.begin();
  xSemaphoreTake(mutex_v, portMAX_DELAY);
  Wire.requestFrom(itsAddr, 2);  // requests 2 bytes

  while (Wire.available() < 2)
    ;  // while two bytes to receive
  // Set em
  adc_high = Wire.read();
  adc_low = Wire.read();
  xSemaphoreGive(mutex_v);
  // now assemble them, remembering our byte maths a Union works well here as well
  adc_result = (adc_high * 256) + adc_low;
  return adc_result;
}

int PH_Probe::loadCalibrationData() {
  Preferences config;
  if (!config.begin("ph_probe", true)) {
    config.begin("ph_probe", false);
    config.end();
    return PROBE_STORAGE_ERROR;
  }
  if (config.getFloat("calTemp", -1) < 0) {
    config.end();
    return PROBE_CONFIG_READ_ERROR;
  } else {
    calTemp = config.getFloat("calTemp");
  }
  if (config.getInt("cal1adc", -1) == -1) {
    config.end();
    return PROBE_STORAGE_ERROR;
  } else {
    cal1adc = config.getInt("cal1adc");
  }
  if (config.getFloat("cal1ph", -1) < 0) {
    config.end();
    return PROBE_STORAGE_ERROR;
  } else {
    cal1ph = config.getFloat("cal1ph");
  }
  if (config.getInt("cal2adc", -1) == -1) {
    config.end();
    return PROBE_STORAGE_ERROR;
  } else {
    cal2adc = config.getInt("cal2adc");
  }
  if (config.getFloat("cal2ph", -1) < 0) {
    config.end();
    return PROBE_STORAGE_ERROR;
  } else {
    cal2ph = config.getFloat("cal2ph");
  }
  config.end();
  return PROBE_OK;
}

int PH_Probe::saveCalibrationData() {
  Preferences config;
  if (!config.begin("ph_probe", false)) {
    config.end();
    return PROBE_STORAGE_ERROR;
  }
  if (calTemp < 0)
    config.putFloat("calTemp", 25);
  else
    config.putFloat("calTemp", calTemp);
  if (cal1adc > 0)
    config.putInt("cal1adc", cal1adc);
  if (cal1ph > 0)
    config.putFloat("cal1ph", cal1ph);
  if (cal2adc > 0)
    config.putInt("cal2adc", cal2adc);
  if (cal2ph > 0)
    config.putFloat("cal2ph", cal2ph);
  Serial.printf(
      "PH_Probe: Saved\
                calTemp = %.2f,\
                cal1adc = %d, cal1ph = %.2f,\
                cal2adc = %d, cal2ph = %.2f\n",
      calTemp,
      cal1adc,
      cal1ph,
      cal2adc,
      cal2ph);
  config.end();
  return PROBE_OK;
}