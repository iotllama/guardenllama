/*
* PH_Probe class usage:
* set cal1ph and cal2ph
* set calTemp to calibration buffer temperature
* put probe in 1st calibration buffer and let probe sit a few minutes 
* get ADC value using the getADCSmooth() method and set cal1adc to this value
* clean probe and repeat procedure for 2nd calibration buffer
* call saveCalibrationData() to save data to flash memory
* When reading ph, the calibration function will read the parameters from flash
*/
#pragma once

#include <Wire.h>

#define PROBE_OK 0
#define PROBE_STORAGE_ERROR 100
#define PROBE_CONFIG_READ_ERROR 200

class PH_Probe {
 public:
  int itsAddr;
  int cal1adc;    // ADC reading for 1st calibration buffer
  float cal1ph;   // pH of 1st calibration buffer
  int cal2adc;    // ADC reading for 2nd calibration buffer
  float cal2ph;   // pH of 2nd calibration buffer
  float calTemp;  // Calibration temperature

  PH_Probe(int addr=0x4D);
  int getADCValue();
  int getADCSmooth();
  float getPH(float temp = 0);
  float getPHSmooth(float temp = 0);
  void calibrate();
  int saveCalibrationData();

 private:
  float phStep;
  TwoWire* itsWire;
  float opaMultiplier = 0.5;  // The VCC multiplier used by the 2nd opamp
  float kelvin = 273.15;
  int loadCalibrationData();
};