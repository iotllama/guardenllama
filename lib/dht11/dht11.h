#pragma once
#include <DHT.h>

class dht11 {
  public:
    dht11(int);
    ~dht11();
    void begin();
    int getTemperature();
    int getHumidity();
  private:
    DHT *sensor;
    const int dhttype = DHT11;
};