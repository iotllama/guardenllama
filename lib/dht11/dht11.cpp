#include <dht11.h>
#include <DHT.h>

dht11::dht11(int gpio) {
  sensor = new DHT(gpio, dhttype);
}

dht11::~dht11() {
  delete(sensor);
}

void dht11::begin() {
  sensor->begin();
}

int dht11::getTemperature() {
  return sensor->readTemperature();
}

int dht11::getHumidity() {
  return sensor->readHumidity();
}