# Intro #

GuardenLlama is a project for monitoring small hydroponics setups. It consists of:
* pH monitoring with temperature compensation
* pH adjustment using a peristaltic pump
* Environment information: air temperature/humidity and water temperature
* Light control/scheduler

This repository hosts the entire project (firmware and schematics)

### Hardware used ###

* ESP32 (Lolin32)
* Nextion NX4832T035 TFT display
* DH11 temperature/humidity sensor
* DS18B20 temperature sensor
* Photoresisor (light sensor)
* pH probe
* miniPH pH probe adc board (see schematics)
* 6V peristaltic pump
* 5V relay module

### Firmware framework ###

The firmware is build on the esp8266-react framework (https://github.com/rjwats/esp8266-react).

The project documentation can be found [here](docs/README.md) 
