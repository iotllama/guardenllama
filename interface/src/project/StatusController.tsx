import React, { Component } from "react";

import { ENDPOINT_ROOT } from "../api";
import { Grid, Container } from "@material-ui/core";
import StatusItemComponent from "./StatusItemComponent";

import ph_img from "./img/ph.png";
import water_temp_img from "./img/water_temp.png";
import humidity from "./img/humidity.png";
import thermometer from "./img/thermometer.png";

export const PH_DATA_ENDPOINT = ENDPOINT_ROOT + "ph";
export const WATER_TEMP_DATA_ENDPOINT = ENDPOINT_ROOT + "waterTemp";
export const AIR_DATA_ENDPOINT = ENDPOINT_ROOT + "airTemp";

class StatusController extends Component {
  render() {
    return (
      <Container>
        <Grid container spacing={2}>
          <Grid item sm>
            <Grid container justify="center" spacing={3}>
              <Grid item>
                <StatusItemComponent
                  img={ph_img}
                  title="Water pH"
                  endpoint={PH_DATA_ENDPOINT}
                />
              </Grid>
              <Grid item>
                <StatusItemComponent
                  img={water_temp_img}
                  title="Water temperature"
                  endpoint={WATER_TEMP_DATA_ENDPOINT}
                />
              </Grid>
            </Grid>
            <Grid container justify="center" spacing={3}>
            <Grid item>
                <StatusItemComponent
                  img={thermometer}
                  title="Air temperature"
                  endpoint={AIR_DATA_ENDPOINT}
                  val="airtemp"
                />
              </Grid>
              <Grid item>
                <StatusItemComponent
                  img={humidity}
                  title="Air temperature"
                  endpoint={AIR_DATA_ENDPOINT}
                  val="airhumidity"
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    );
  }
}

//export default restController(PH_DATA_ENDPOINT, StatusController);
export default StatusController;
