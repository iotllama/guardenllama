import React, { Component } from "react";
import { Card, CardMedia, CardContent, Typography } from "@material-ui/core";
import { WithSnackbarProps, withSnackbar } from "notistack";
import "./StatusCss.css";
import { redirectingAuthorizedFetch } from "../authentication";

interface StatusItemProps extends WithSnackbarProps {
  img: string;
  title: string;
  endpoint: string;
  val?: string;
  func?: (tmp: number) => void;
}

interface StatusItemState {
  value: string;
  caption: string;
}

class StatusItemComponent extends Component<StatusItemProps, StatusItemState> {
  state: StatusItemState = {
    value: "N/A",
    caption: "",
  };

  intervalId: any;

  fetchData = () => {
    redirectingAuthorizedFetch(this.props.endpoint)
      .then((res) => res.json())
      .then((data) => {
        if (this.props.val && data[this.props.val]) {
          console.log(data);
            this.setState({ value: data[this.props.val] });
        } else
        if (data?.value && data?.value != this.state.value) {
          this.setState({ value: data.value, caption: data.tstamp });

          if (this.props.func) this.props.func(data.value);
        }
        this.setState({ caption: data.tstamp });
      })
      .catch((err) => {
        this.props.enqueueSnackbar("Error fetching data: " + err.message, {
          variant: "error",
          preventDuplicate: true,
        });
      });
  };

  componentDidMount() {
    this.intervalId = setInterval(this.fetchData, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  render() {
    return (
      <Card>
        <CardMedia
          src={this.props.img}
          component="img"
          title={this.props.title}
          style={{
            height: "5em",
            width: "5em",
            objectFit: "contain",
            marginLeft: "auto",
            marginRight: "auto",
          }}
        />
        <CardContent>
          <Typography style={{ fontWeight: 900 }} variant="h2">
            {this.state.value}
          </Typography>
          <Typography variant="caption">{this.state.caption}</Typography>
        </CardContent>
      </Card>
    );
  }
}

export default withSnackbar(StatusItemComponent);
