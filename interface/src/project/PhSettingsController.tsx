import React, { Component } from "react";
import {
  Grid,
  Container,
  TextField,
  Theme,
  createStyles,
  WithStyles,
  withStyles,
  Fab,
} from "@material-ui/core";
import "./SettingsCss.css";
import StatusItemComponent from "./StatusItemComponent";
import { PH_DATA_ENDPOINT, WATER_TEMP_DATA_ENDPOINT } from "./StatusController";
import ph_img from "./img/ph.png";
import water_temp_img from "./img/water_temp.png";
import { WithSnackbarProps, withSnackbar } from "notistack";
import { CheckCircleOutlineOutlined, ThreeSixty } from "@material-ui/icons";
import { ENDPOINT_ROOT } from "../api";
import { redirectingAuthorizedFetch } from "../authentication";
import target from "./img/target.png";
export const PHSETTINGS_ENDPOINT = ENDPOINT_ROOT + "ph_calibration";

const styles = (theme: Theme) =>
  createStyles({
    environment: {
      marginTop: theme.spacing(6),
    },
    button: {
      marginRight: theme.spacing(2),
      marginTop: theme.spacing(2),
    },
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
    root: {
      marginTop: theme.spacing(6),
      "& .MuiTextField-root": {
        margin: theme.spacing(1),
        width: "25ch",
      },
    },
  });

type PhSettingsControllerProps = WithSnackbarProps & WithStyles<typeof styles>;

interface PhSettingsControllerState {
  cal1ph: number | string;
  cal2ph: number | string;
  calpt: number | string;
  caltemp: number | string;
}

class PhSettingsController extends Component<
  PhSettingsControllerProps,
  PhSettingsControllerState
> {
  state = {
    cal1ph: 0,
    cal2ph: 0,
    calpt: 0,
    caltemp: 0,
  };
  componentDidMount() {
    redirectingAuthorizedFetch(PHSETTINGS_ENDPOINT)
      .then((res) => res.json())
      .then((data) => {
        this.setState({
          cal1ph: data.cal1ph,
          cal2ph: data.cal2ph,
        });
      })
      .catch((err) => {
        this.props.enqueueSnackbar("Error fetching data: " + err.message, {
          variant: "error",
          preventDuplicate: true,
        });
      });
    this.setState({ cal1ph: 3.45 });
    this.setState({ cal2ph: 5.67 });
  }

  getTemp = (temp: number) => {
    this.setState((prevState, props) => {
      return {
        caltemp: temp,
      };
    });
  };

  handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const calpt = event.currentTarget.id;
    var _state = this.state;
    _state.calpt = parseInt(calpt);
    redirectingAuthorizedFetch(PHSETTINGS_ENDPOINT, {
      method: "POST",
      body: JSON.stringify(_state),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        }
        throw Error("Invalid status code: " + response.status);
      })
      .then((json) => {
        this.props.enqueueSnackbar("Update successful.", {
          variant: "success",
        });
        this.setState(json);
      })
      .catch((error) => {
        const errorMessage = error.message || "Unknown error";
        this.props.enqueueSnackbar("Problem updating: " + errorMessage, {
          variant: "error",
        });
      });
  };

  handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.currentTarget;
    const value = target.value;
    const name = target.name;
    if (name === "cal1ph") {
      this.setState({
        cal1ph: value,
      });
    }
    if (name === "cal2ph") {
      this.setState({
        cal2ph: value,
      });
    }
    if (name === "calpt") {
      this.setState({
        calpt: value,
      });
    }
  };
  render() {
    const { classes } = this.props;
    return (
      <Container className={classes.root}>
        <Grid container spacing={3} justify="center">
        
          <Grid item xs={5}>
            <form id="1" onSubmit={this.handleSubmit}>
              <TextField
                name="cal1ph"
                color="secondary"
                label="pH"
                type="number"
                inputProps={{ min: "1", max: "12", step: "0.01" }}
                InputLabelProps={{ shrink: true }}
                variant="outlined"
                helperText="Calibration solution 1"
                value={this.state.cal1ph}
                onChange={this.handleInputChange}
              />
              <Fab
                type="submit"
                value="1"
                color="secondary"
                variant="extended"
                aria-label="save"
                className={classes.button}
              >
                <CheckCircleOutlineOutlined className={classes.extendedIcon} />
                Save calibration
              </Fab>
            </form>
          </Grid>
          <Grid item xs={5}>
            <form id="2" onSubmit={this.handleSubmit}>
              <TextField
                name="cal2ph"
                color="secondary"
                label="pH"
                type="number"
                inputProps={{ min: "1", max: "12", step: "0.01" }}
                InputLabelProps={{ shrink: true }}
                variant="outlined"
                value={this.state.cal2ph}
                helperText="Calibration solution 2"
                onChange={this.handleInputChange}
              />
              <Fab
                type="submit"
                value="2"
                color="secondary"
                variant="extended"
                aria-label="save"
                className={classes.button}
              >
                <CheckCircleOutlineOutlined className={classes.extendedIcon} />
                Save calibration
              </Fab>
            </form>
          </Grid>
         
        </Grid>

        <Grid container spacing={3} justify="center">
          <Grid item xs={4}>
            <StatusItemComponent
              endpoint={PH_DATA_ENDPOINT}
              title="pH value"
              img={ph_img}
            />
          </Grid>
          <Grid item xs={4}>
            <StatusItemComponent
              endpoint={PHSETTINGS_ENDPOINT}
              title="raw value"
              img={target}
              val="adc"
            />
          </Grid>
          
          <Grid item xs={4}>
            <StatusItemComponent
              endpoint={WATER_TEMP_DATA_ENDPOINT}
              title="Water temperature"
              img={water_temp_img}
              func={this.getTemp}
            />
           
          </Grid>
        </Grid>
      </Container>
    );
  }
}

export default withSnackbar(withStyles(styles)(PhSettingsController));
