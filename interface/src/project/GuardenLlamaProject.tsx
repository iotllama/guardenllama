import React, { Component } from 'react';
import { Redirect, Switch, RouteComponentProps } from 'react-router-dom'

import { Tabs, Tab } from '@material-ui/core';

import { PROJECT_PATH } from '../api';
import { MenuAppBar } from '../components';
import { AuthenticatedRoute } from '../authentication';
import  StatusController  from './StatusController';
import { SettingsOutlined, HomeOutlined } from '@material-ui/icons';
import PhSettingsController from './PhSettingsController';



class GuardenLlamaProject extends Component<RouteComponentProps> {

  handleTabChange = (event: React.ChangeEvent<{}>, path: string) => {
    this.props.history.push(path);
  };

  render() {
    return (
      <MenuAppBar sectionTitle="Guarden Llama">
        <Tabs value={this.props.match.url} onChange={this.handleTabChange} variant="fullWidth" indicatorColor="primary"  >
          <Tab value={`/${PROJECT_PATH}/llama/status`} icon={<HomeOutlined/>} label="Status" />
          <Tab value={`/${PROJECT_PATH}/llama/phsettings`} icon={<SettingsOutlined/>} label="pH Probe" />
        </Tabs>
        <Switch>
          <AuthenticatedRoute exact path={`/${PROJECT_PATH}/llama/status`} component={StatusController} />
          <AuthenticatedRoute exact path={`/${PROJECT_PATH}/llama/phsettings`} component={PhSettingsController} />
          <Redirect to={`/${PROJECT_PATH}/llama/status`} />
        </Switch>
      </MenuAppBar>
    )
  }

}

export default GuardenLlamaProject;
