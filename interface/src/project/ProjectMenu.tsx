import React, { Component } from 'react';
import { Link, withRouter, RouteComponentProps } from 'react-router-dom';

import {List, ListItem, ListItemIcon, ListItemText} from '@material-ui/core';
import LocalFloristIcon from '@material-ui/icons/LocalFloristOutlined';

import { PROJECT_PATH } from '../api';

class ProjectMenu extends Component<RouteComponentProps> {

  render() {
    const path = this.props.match.url;
    return (
      <List>
        <ListItem to={`/${PROJECT_PATH}/llama/`} selected={path.startsWith(`/${PROJECT_PATH}/llama/`)} button component={Link}>
          <ListItemIcon>
            <LocalFloristIcon />
          </ListItemIcon>
          <ListItemText primary="Guarden Llama" />
        </ListItem>
      </List>
    )
  }

}

export default withRouter(ProjectMenu);
