EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:+5V #PWR0101
U 1 1 5F5216D1
P 1650 1500
F 0 "#PWR0101" H 1650 1350 50  0001 C CNN
F 1 "+5V" H 1665 1673 50  0000 C CNN
F 2 "" H 1650 1500 50  0001 C CNN
F 3 "" H 1650 1500 50  0001 C CNN
	1    1650 1500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5F521CF6
P 2050 1900
F 0 "#PWR0102" H 2050 1650 50  0001 C CNN
F 1 "GND" H 2055 1727 50  0000 C CNN
F 2 "" H 2050 1900 50  0001 C CNN
F 3 "" H 2050 1900 50  0001 C CNN
	1    2050 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 1650 1550 1650
Text GLabel 1650 1650 2    50   Input ~ 0
5V
Text GLabel 2050 1750 2    50   Input ~ 0
GND
$Comp
L MyLibs:Wemos_LoLin32 U1
U 1 1 5F533033
P 3400 2500
F 0 "U1" H 3400 3803 60  0000 C CNN
F 1 "Wemos_LoLin32" H 3400 3697 60  0000 C CNN
F 2 "Wemos:LoLin_32_Board" H 3950 2050 60  0001 C CNN
F 3 "http://www.wemos.cc/Products/d1_mini.html" H 3500 3600 60  0000 C CNN
	1    3400 2500
	1    0    0    -1  
$EndComp
Text GLabel 2900 3050 0    50   Input ~ 0
GND
Text GLabel 3900 3250 2    50   Input ~ 0
GND
Text GLabel 3900 1550 2    50   Input ~ 0
GND
Text GLabel 2900 2950 0    50   Input ~ 0
5V
Text GLabel 3900 2150 2    50   Input ~ 0
GND
Wire Wire Line
	3900 2250 3900 2150
Text GLabel 3900 1850 2    50   Input ~ 0
3V3
NoConn ~ 2900 1650
NoConn ~ 2900 1750
NoConn ~ 2900 1850
NoConn ~ 2900 1950
NoConn ~ 2900 2050
NoConn ~ 2900 2150
NoConn ~ 2900 2250
NoConn ~ 2900 2450
NoConn ~ 2900 2550
NoConn ~ 2900 2650
NoConn ~ 2900 2750
NoConn ~ 2900 2850
NoConn ~ 3900 3450
NoConn ~ 3900 3350
NoConn ~ 3900 3050
NoConn ~ 3900 2950
NoConn ~ 3900 2850
NoConn ~ 3900 2750
NoConn ~ 3900 2650
NoConn ~ 3900 2550
NoConn ~ 3900 2450
NoConn ~ 3900 2350
NoConn ~ 3900 1750
NoConn ~ 3900 1650
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 5F53B09F
P 1300 3250
F 0 "J2" H 1380 3242 50  0000 L CNN
F 1 "pH" H 1380 3151 50  0000 L CNN
F 2 "Connectors_Molex:Molex_KK-6410-04_04x2.54mm_Straight" H 1300 3250 50  0001 C CNN
F 3 "~" H 1300 3250 50  0001 C CNN
	1    1300 3250
	-1   0    0    1   
$EndComp
Text GLabel 1500 3350 2    50   Input ~ 0
SCL
Text GLabel 1500 3250 2    50   Input ~ 0
SDA
Text GLabel 1500 3150 2    50   Input ~ 0
3V3
Text GLabel 1500 3050 2    50   Input ~ 0
GND
$Comp
L Device:R R1
U 1 1 5F53E687
P 4350 1800
F 0 "R1" H 4280 1754 50  0000 R CNN
F 1 "3.3k" H 4280 1845 50  0000 R CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4280 1800 50  0001 C CNN
F 3 "~" H 4350 1800 50  0001 C CNN
	1    4350 1800
	-1   0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 5F53F8DA
P 4350 2200
F 0 "R2" H 4420 2246 50  0000 L CNN
F 1 "3.3k" H 4420 2155 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4280 2200 50  0001 C CNN
F 3 "~" H 4350 2200 50  0001 C CNN
	1    4350 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 2050 4350 2050
Wire Wire Line
	3900 1950 4350 1950
Text GLabel 4350 1650 1    50   Input ~ 0
3V3
Text GLabel 4350 2350 3    50   Input ~ 0
3V3
Text GLabel 4550 1950 2    50   Input ~ 0
SCL
Wire Wire Line
	4550 1950 4350 1950
Connection ~ 4350 1950
Text GLabel 4550 2050 2    50   Input ~ 0
SDA
Wire Wire Line
	4350 2050 4550 2050
Connection ~ 4350 2050
NoConn ~ 2900 1550
Text Notes 7400 7500 0    79   ~ 16
GuardenLlama
Text Notes 8150 7650 0    50   ~ 0
04/09/2020
Text Notes 10600 7650 0    50   ~ 0
0.0.2
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 5F5673ED
P 1300 2500
F 0 "J3" H 1380 2542 50  0000 L CNN
F 1 "DS18B20" H 1380 2451 50  0000 L CNN
F 2 "Connectors_Molex:Molex_KK-6410-03_03x2.54mm_Straight" H 1300 2500 50  0001 C CNN
F 3 "~" H 1300 2500 50  0001 C CNN
	1    1300 2500
	-1   0    0    1   
$EndComp
Text GLabel 2050 2500 2    50   Input ~ 0
3V3
Text GLabel 2050 2400 2    50   Input ~ 0
GND
$Comp
L Device:R R3
U 1 1 5F5682B2
P 1750 2600
F 0 "R3" V 1700 2400 50  0000 C CNN
F 1 "10k" V 1750 2600 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 1680 2600 50  0001 C CNN
F 3 "~" H 1750 2600 50  0001 C CNN
	1    1750 2600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2050 2400 1500 2400
Wire Wire Line
	2050 2500 1900 2500
Wire Wire Line
	1900 2600 1900 2500
Connection ~ 1900 2500
Wire Wire Line
	1900 2500 1500 2500
Wire Wire Line
	1600 2600 1550 2600
Text GLabel 1550 2750 2    50   Input ~ 0
Temp1
Wire Wire Line
	1550 2750 1550 2600
Connection ~ 1550 2600
Wire Wire Line
	1550 2600 1500 2600
Text GLabel 2900 2350 0    50   Input ~ 0
Temp1
Wire Wire Line
	2050 1900 2050 1750
Wire Wire Line
	1650 1500 1650 1650
$Comp
L Connector_Generic:Conn_01x03 J4
U 1 1 5F6623B0
P 1300 3800
F 0 "J4" H 1380 3842 50  0000 L CNN
F 1 "DH11" H 1380 3751 50  0000 L CNN
F 2 "Connectors_Molex:Molex_KK-6410-03_03x2.54mm_Straight" H 1300 3800 50  0001 C CNN
F 3 "~" H 1300 3800 50  0001 C CNN
	1    1300 3800
	-1   0    0    1   
$EndComp
Text GLabel 1500 3900 2    50   Input ~ 0
3V3
Text GLabel 1500 3800 2    50   Input ~ 0
DH11
Text GLabel 1500 3700 2    50   Input ~ 0
GND
Text GLabel 3900 3150 2    50   Input ~ 0
DH11
Wire Wire Line
	1500 1750 1600 1750
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5F8616B6
P 1300 1750
F 0 "J1" H 1450 1550 50  0000 C CNN
F 1 "Conn_01x02" H 1650 1650 50  0000 C CNN
F 2 "Connectors_Molex:Molex_KK-6410-02_02x2.54mm_Straight" H 1300 1750 50  0001 C CNN
F 3 "~" H 1300 1750 50  0001 C CNN
	1    1300 1750
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5F86CF8D
P 1550 1650
F 0 "#FLG0101" H 1550 1725 50  0001 C CNN
F 1 "PWR_FLAG" H 1400 1800 50  0000 C CNN
F 2 "" H 1550 1650 50  0001 C CNN
F 3 "~" H 1550 1650 50  0001 C CNN
	1    1550 1650
	1    0    0    -1  
$EndComp
Connection ~ 1550 1650
Wire Wire Line
	1550 1650 1650 1650
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5F870462
P 1600 1750
F 0 "#FLG0102" H 1600 1825 50  0001 C CNN
F 1 "PWR_FLAG" H 1450 1900 50  0000 C CNN
F 2 "" H 1600 1750 50  0001 C CNN
F 3 "~" H 1600 1750 50  0001 C CNN
	1    1600 1750
	-1   0    0    1   
$EndComp
Connection ~ 1600 1750
Wire Wire Line
	1600 1750 2050 1750
$EndSCHEMATC
