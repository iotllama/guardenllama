# Sparky's miniPH ADC Board Notes

## Internal workings
* VCC =VRef = 3.325V out of Lolin32 ESP32 board
* Charge pump negative output voltage Vneg = -3.312V
* opamp rails connected to VCC and Vneg
* First stage gain in a non-inverting configuration given by R8=200K and R7=47K => 5.255
* Second stage is a differential configuration.
  * Non-inverting input 2 is connected to the voltage divider formed by R13=1K and R12=3K
  * Inverting input is connected to the output of the first stage after R9=220K
  * Feedback resistor R10=220K
 * Output voltage of stage 2 => -Vout1 + Vcc * 0.5

## Limitations
 * Because the output of stage 2 used as input for the ADC => the maximum Vout1 voltage we can use is **1.66V**. Anything greater thant that will generate negative Vout2 and will result in an ADC output value of 0.
 * The minimum Vout1 voltage is **-1.66V**. Anything lower than this will max out the Vout2 and will result in a 4095 ADC output value.

### Theoretical pH range
* Vstep = -59.14mv
* For Vout1 = 1.66V => Vprobe = 0.3162V => deltaPhMax = 5.347
  * phMin = 7-5.347 = 1.653
  * phMax = 12.35
* Good enough for hydroponics

## pH Calculation

### Calibration
* will use 2 point calibration w/ standard pH buffers: 4.01 and 6.86
* read ADC values for both calibration solutions
* calculate slope (x-axis = adc value, y-axis = pH): m=(ph1-ph2)/(adc1-adc2)=delta_ph_buffers/delta_adc_buffers
* calculate y-intercept: B = y - mx = ph1 - m*adc1

### pH Calculation
For an adc reading *ADC* of unknown pH, we can use the equation y(pH)=mx+b (above). If we plug in B, we get y=mx + ph1 - m*adc1 => *y=m(x-adc1) + ph1*

### Temperature compensation
For hydroponics temperature pH compensation is overkill because nutrient solutions are kept somewhere around 25C and the pH of hydroponic nutrient solutions should be allowed to move between certain ranges as nutrient root absorbtion rates for certain nutrients are pH dependent.

For science, let's do it anyway. We need to make the equation dependent on temperature somehow. For an ideal probe which has a slope of -59.14 mv/Ph we could use the Nernst equation. Real life probes, especially cheap ones, are not ideal and they do not output 0mV at pH 7. The following calculations are based on "Theory and Practice of pH Measurement" from Rosemount Analytical. There's a good chance that I'm totally wrong about this stuff :).

Let's make the slope temperature dependent on the temperature in Kelvin(K): m/K=(ph1-ph2)/((adc1-adc2)*K0), where K0 = tCelsius + 273.15 at calibration time.

The pH line equation becomes: y = m*K*(x-adc1) + ph1, where K=temp in Kelvin when the measurement is taken. Essentially we multiply the slope by a factor of K/K0.

### Test calculations with chinese pH probe
```
Temperature = 23C = 296.25K
ADC1 = 900, pH1 = 4.01
ADC2 = 1973, pH1 = 6.86
m = (6.86 - 4.01) / (1973 - 900) = 2.85/1073
b = 4.01 - 2.85/1073 * 900 = 1.619
pH = 2.85/1073(adc - 900)+ 4.01
pH[temp] = 2.85/1073 * TempK/296.25 *(adc-900) + 4.01
```
* for ADC = 1973 => Ph = 2.85/1073(1973-900) + 4.01 = 6.86
* for ADC = 1973@25C => pH = 2.85/1073 \* 298.15/296.15 \* (1973-900) + 401 = 6.88

-----



-----

# Battery Backup Notes
Need a way to keep the ESP32 RTC going when unpluggig the device. This is not too much of an issue when Wifi and NTP are enabled but setting the time after every power cycle can be a pain in the ass.
That is why I would like to add some kind of battery backup/UPS for the device.

Target:
* charge battery when external 5V power is connected
* battery should provide 5V to system
* battery should be protected against over charging and over discharging
* battery should take over when 5V external power is disconnected

A sample circtuit using the TP4056+FS312F+MT3608 can be found here: https://easyeda.com/GreatScott/LiPoChargeProtectBoost_copy-3d9f4c3ddc7347d7861776340b9b90b7

For the powerpath, the idea is to use a PMOS to stop current from the boost converter when the power supply is connected. Found a TI paper which discusses different solutions (https://www.ti.com/lit/pdf/slua376).
The approach to test is described in "Topology 4" (using diodes and MOSFET).

## Charging
### TP4056 ($0.05)
* Datasheet: https://dlnmh9ip6v2uc.cloudfront.net/datasheets/Prototyping/TP4056.pdf
* 2.9V Trickle Charge Threshold (TP4056) 
* Programmable Charge Current Up to 1000mA 

## Protection
### FS312F ($0.10)
* https://ic-fortune.com/upload/Download/FS312F-G-DS-12_EN.pdf
* Overcharge protection 4.25V
* Overdischarge protection 2.90V
* Functions by disconnecting ground via 2 NMOS

## Boost converter 
### MT3608 ($0.33)
* https://datasheet.lcsc.com/szlcsc/1811151539_XI-AN-Aerosemi-Tech-MT3608_C84817.pdf
*  Integrated 80mΩ Power MOSFET
* 2V to 24V Input Voltage
* Internal 4A Switch Current Limit
* Up to 28V Output Voltage

### TPS6123x ($1.99)
* https://www.ti.com/lit/ds/symlink/tps61230.pdf?ts=1606395501676&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FTPS61230%253FkeyMatch%253DTPS61230DRC%2526tisearch%253DSearch-EN-everything%2526usecase%253DGPN
* Input Voltage Range: 2.3 V to 5.5 V
* Output Voltage Range: 2.5 V to 5.5 V
* Input supply voltage supervisor w/ adjustable hysteresis


