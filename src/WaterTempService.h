#pragma once

#include <HttpEndpoint.h>
#include <DS18B20.h>

#define WATER_TEMP_ENDPOINT "/rest/waterTemp"

class waterTempState {
  public:
    static float waterTemp;
    static String tstamp;

    static void read(waterTempState& settings, JsonObject& root);
};

class WaterTempService: public StatefulService<waterTempState> {
  public:
    WaterTempService(AsyncWebServer* server, SecurityManager* securityManager, int gpio);
    ~WaterTempService();
    void begin();
    void updateWaterTemp();
  private:
    DS18B20* tempProbe;
    waterTempState _waterTempState;
    HttpGetEndpoint<waterTempState> _httpEndpoint;
};