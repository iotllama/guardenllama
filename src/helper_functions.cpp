#include <helper_functions.h>
#include <Arduino.h>


String getTimeStamp() {
  const time_t now = time(nullptr);
  if(!isTimeSet()) 
    return "N/A";
  char time_string[25];
  strftime(time_string, 25, "%T %d/%m/%Y", localtime(&now));
  return time_string;
}

bool isTimeSet() {
  const time_t now = time(nullptr);
  char time_string[5];
  strftime(time_string, 5, "%Y", localtime(&now));
  return (strcmp(time_string, "1970") > 0);
}