#pragma once

#include <HttpEndpoint.h>

#define LIGHT_SETTINGS_ENDPOINT_PATH "/rest/lightState"

#define OFF_STATE "OFF"
#define ON_STATE "ON"
#define LED_ON 0x0
#define LED_OFF 0x1

class Light {
 public:
  bool ledOn;

  static void read(Light& settings, JsonObject& root) {
    root["led_on"] = settings.ledOn;
  }

  static void haRead(Light& settings, JsonObject& root) {
    root["state"] = settings.ledOn ? ON_STATE : OFF_STATE;
  }

  static StateUpdateResult update(JsonObject& root, Light& lightState) {
    boolean newState = root["led_on"];
    if (lightState.ledOn != newState) {
      lightState.ledOn = newState;
      return StateUpdateResult::CHANGED;
    }
    return StateUpdateResult::UNCHANGED;
  }
};

class LightService : public StatefulService<Light> {
  public:
    LightService(AsyncWebServer* server,
                 SecurityManager* securityManager);
    void begin();
    void turnOn();
    void turnOff();
  
  private:
    HttpGetEndpoint<Light> _httpEndpoint;

    void onConfigUpdated();

};
