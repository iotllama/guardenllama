#include <pHService.h>
#include "helper_functions.h"
#include <Arduino.h>

float pHState::pHValue = -1;
float pHState::cal1ph = -1;
float pHState::cal2ph = -1;
float pHState::caltemp = 25;
int pHState::calpt = 0;
int pHState::adc = 0;
String pHState::tstamp = "NA";
PH_Probe pHCalibrationService::pHProbe;

void pHState::readall(pHState& settings, JsonObject& root) {
  root["value"] = String(pHValue, 1);
  root["cal1ph"] = settings.cal1ph;
  root["cal2ph"] = settings.cal2ph;
  root["adc"] = settings.adc;
}

void pHState::readph(pHState& settings, JsonObject& root) {
  root["value"] = String(pHValue, 1);
  root["tstamp"] = getTimeStamp();
}

StateUpdateResult pHState::update(JsonObject& root, pHState& settings) {
  float cal1ph = root["cal1ph"].as<float>();
  float cal2ph = root["cal2ph"].as<float>();
  float caltemp = root["caltemp"].as<float>();
  int calpt = root["calpt"].as<int>();

  Serial.printf("cal1ph = %.2f, cal2ph = %.2f, caltemp = %.2f, calpt =%d\n", cal1ph, cal2ph, caltemp, calpt);
  // if (!(cal1ph != 0 && cal2ph != 0 && caltemp != 0) || (calpt == 0))
  if (calpt == 0)
    return StateUpdateResult::ERROR;

  settings.cal1ph = cal1ph;
  settings.cal2ph = cal2ph;
  settings.caltemp = caltemp;
  settings.calpt = calpt;
  return StateUpdateResult::CHANGED;
}

pHService::pHService(AsyncWebServer* server, SecurityManager* securityManager, int addr) :
    _httpEndpoint(pHState::readph,
                  this,
                  server,
                  PH_ENDPOINT,
                  securityManager,
                  AuthenticationPredicates::NONE_REQUIRED) {
}

void pHCalibrationService::updateCalibrationParams(float cal1ph,
                                                   float cal2ph,
                                                   int cal1adc,
                                                   int cal2adc,
                                                   float caltemp) {
  if (cal1adc > 0) {
    pHProbe.cal1adc = cal1adc;
    pHProbe.cal1ph = cal1ph;
    pHProbe.calTemp = caltemp;
  } else if (cal2adc > 0) {
    pHProbe.cal2adc = cal2adc;
    pHProbe.cal2ph = cal2ph;
    pHProbe.calTemp = caltemp;
  }
  pHProbe.saveCalibrationData();
}

void pHCalibrationService::loop() {
  _state.adc = pHProbe.getADCSmooth();
  _state.pHValue = pHProbe.getPHSmooth();
}

pHCalibrationService::pHCalibrationService(AsyncWebServer* server, SecurityManager* securityManager, int addr) :
    _httpEndpoint(pHState::readall,
                  pHState::update,
                  this,
                  server,
                  PH_CALIB_ENDPOINT,
                  securityManager,
                  AuthenticationPredicates::IS_ADMIN) {
  pHProbe = PH_Probe(addr);
  // configure settings service update handler to update LED state
  addUpdateHandler([&](const String& originId) { updateConfig(); }, false);
}

void pHCalibrationService::begin() {
  pHProbe.calibrate();
  _state.cal1ph = pHProbe.cal1ph;
  _state.cal2ph = pHProbe.cal2ph;
  _state.caltemp = pHProbe.calTemp;
  loop();
}

void pHCalibrationService::updateConfig() {
  pHProbe.cal1ph = _state.cal1ph;
  pHProbe.cal2ph = _state.cal2ph;
  pHProbe.calTemp = _state.caltemp;
  if (_state.calpt == 1) {
    pHProbe.cal1adc = _state.adc;
  } else if (_state.calpt == 2)
    pHProbe.cal2adc = _state.adc;
  pHProbe.saveCalibrationData();
}