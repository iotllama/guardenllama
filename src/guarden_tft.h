#pragma once
#include <Arduino.h>
#include <Nextion.h>
#include <time.h>
#include <pHService.h>
#include <WaterTempService.h>
#include <AirTempService.h>
#include <Preferences.h>
#include <soc/timer_group_struct.h>
#include <soc/timer_group_reg.h>

struct Settings {
  int screen_timeout = 60;
  int brightness = 100;
  bool wifi_on = true;
};

Nextion nex(Serial2, 115200);
static boolean tft_enable = false;
static Settings s;
int lastDateTimeUpdate = 0;
boolean isSleeping = false;
static int currPage = 1;
static String _tft_tmstamp = "";
TaskHandle_t tftUpdates;

void setup_tft();
void read_settings();
bool save_preferences();
bool updateWtTemp();
void updateStats();
void updateDateTime();
void populateSettingsPage();
void populatePhCalibrationPage();
void updatePhCalibrationPage();
void toggleConfigDateTime();
void saveConfiguration();
void saveCalibrationConfig(int calpt);
void setRTC();
void tft_loop();
void tft_task(void* param);

void saveCalibrationConfig(int calpt) {
  if (calpt == 1) {
    float cal1ph = nex.getComponentValue("x0") / 100.0;
    int cal1adc = pHState::adc;
    float temp = waterTempState::waterTemp;
    pHCalibrationService::updateCalibrationParams(cal1ph, 0, cal1adc, 0, temp);
    pHState::cal1ph = cal1ph;
    pHState::caltemp = temp;
  }
  if (calpt == 2) {
    float cal2ph = nex.getComponentValue("x1") / 100.0;
    int cal2adc = pHState::adc;
    float temp = waterTempState::waterTemp;
    Serial.printf("cal2ph=%.2f, cal2adc=%d\n", cal2ph, cal2adc);
    pHCalibrationService::updateCalibrationParams(0, cal2ph, 0, cal2adc, temp);
    pHState::cal2ph = cal2ph;
    pHState::caltemp = temp;
  }
}

void populatePhCalibrationPage() {
  int cal1Val = round(pHState::cal1ph * 100);
  int cal2Val = round(pHState::cal2ph * 100);
  int adcValue = pHState::adc;
  nex.setComponentValue("x0", cal1Val);
  if (cal1Val > 999)
    nex.sendCommand("x0.vvs0=2");
  else
    nex.sendCommand("x0.vvs0=1");
  nex.setComponentValue("x1", cal2Val);
  if (cal2Val > 999)
    nex.sendCommand("x1.vvs0=2");
  else
    nex.sendCommand("x1.vvs0=1");
  nex.setComponentValue("n0", adcValue);
}

void updatePhCalibrationPage() {
  if (currPage != 4)
    return;
  nex.setComponentValue("n0", pHState::adc);
  nex.setComponentValue("n1", round(pHState::pHValue * 10));
}

void setRTC() {
  struct tm timeinfo;
  String cmd = nex.getComponentText("t1");
  timeinfo.tm_hour = nex.getComponentText("t1").toInt();
  timeinfo.tm_min = nex.getComponentText("t2").toInt();
  timeinfo.tm_sec = 0;
  timeinfo.tm_mday = nex.getComponentText("t4").toInt();
  timeinfo.tm_mon = nex.getComponentText("t6").toInt() - 1;
  timeinfo.tm_year = nex.getComponentText("t8").toInt() - 1900;
  time_t t = mktime(&timeinfo);
  struct timeval now = {.tv_sec = t};
  settimeofday(&now, NULL);
  String currtz = getenv("TZ");
  if (currtz != NULL) {
    setenv("TZ", currtz.c_str(), 1);
  }
}

void saveConfiguration() {
  int _tmp = nex.getComponentValue("n0");
  if (_tmp < 0)
    return;
  s.screen_timeout = _tmp;

  _tmp = nex.getComponentValue("n1");
  if (_tmp < 0)
    return;
  s.brightness = _tmp;

  _tmp = nex.getComponentValue("c0");
  if (_tmp < 0)
    return;
  s.wifi_on = _tmp;
  if (!s.wifi_on) {
    setRTC();
  }

  if (save_preferences()) {
    nex.sendCommand("dp=2");
    currPage = 2;
  }
}

void populateSettingsPage() {
  read_settings();
  const time_t now = time(nullptr);
  char hours[3];
  strftime(hours, 3, "%H", localtime(&now));
  char mins[3];
  strftime(mins, 3, "%M", localtime(&now));
  char day[3];
  strftime(day, 3, "%d", localtime(&now));
  char mon[3];
  strftime(mon, 3, "%m", localtime(&now));
  char year[5];
  strftime(year, 5, "%Y", localtime(&now));
  nex.setComponentText("t1", hours);
  nex.setComponentText("t2", mins);
  nex.setComponentText("t4", day);
  nex.setComponentText("t6", mon);
  nex.setComponentText("t8", year);

  nex.setComponentValue("n0", s.screen_timeout);
  nex.setComponentValue("n1", s.brightness);
  nex.setComponentValue("c0", s.wifi_on);
  toggleConfigDateTime();
}

void toggleConfigDateTime() {
  int val = nex.getComponentValue("c0");
  if (val > 0) {
    for (int i = 0; i < 10; i++) {
      String cmd = "vis m";
      cmd += i;
      cmd += ",0";
      nex.sendCommand(cmd.c_str());
    }
    for (int i = 1; i < 9; i++) {
      String cmd = "t";
      cmd += i;
      cmd += ".pco=46518";
      nex.sendCommand(cmd.c_str());
    }
  } else {
    for (int i = 0; i < 10; i++) {
      String cmd = "vis m";
      cmd += i;
      cmd += ",1";
      nex.sendCommand(cmd.c_str());
    }
    for (int i = 1; i < 9; i++) {
      String cmd = "t";
      cmd += i;
      cmd += ".pco=32776";
      nex.sendCommand(cmd.c_str());
    }
  }
}

void updateDateTime() {
  const time_t now = time(nullptr);
  char time_string[20];
  strftime(time_string, 20, "%d/%m/%Y %H:%M", localtime(&now));
  if (_tft_tmstamp != time_string) {
    nex.setComponentText("page1.t0", time_string);
    nex.setComponentText("page2.t0", time_string);
    _tft_tmstamp = time_string;
  }
}

void updateStats() {
  if (currPage != 1)
    return;
  int tmp = nex.getComponentValue("x0");
  if (tmp < 0)
    nex.setComponentValue("x0", 99);
  int phVal = round(pHState::pHValue * 10.0);
  if (phVal != tmp)
    nex.setComponentValue("x0", phVal);

  tmp = nex.getComponentValue("x1");
  int wtTemp = round(waterTempState::waterTemp * 10);
  if (wtTemp < 0) {
    wtTemp = 0;
  }
  if (wtTemp != tmp)
    nex.setComponentValue("x1", wtTemp);

  tmp = nex.getComponentValue("n0");
  int _temp = airTempState::airTemp;
  if (_temp < 0)
    _temp = 0;
  if (_temp != tmp)
    nex.setComponentValue("n0", _temp);

  tmp = nex.getComponentValue("n1");
  int _hum = airTempState::airHumidity;
  if (_hum < 0)
    _hum = 0;
  if (_hum != tmp)
    nex.setComponentValue("n1", _hum);
}

bool save_preferences() {
  Preferences settings;
  if (!settings.begin("settings", false))
    return false;
  settings.putInt("timeout", s.screen_timeout);
  settings.putInt("brightness", s.brightness);
  settings.putBool("wifi", s.wifi_on);
  settings.end();
  return true;
}

void read_settings() {
  Preferences settings;
  if (!settings.begin("settings", true)) {
    save_preferences();
    return;
  }
  s.brightness = settings.getInt("brightness", 100);
  s.screen_timeout = settings.getInt("timeout", 0);
  s.wifi_on = settings.getBool("wifi", true);
}

void setup_tft() {
  if (nex.init("1")) {
    isSleeping = false;
    currPage = 1;
  }
  read_settings();
  String cmd = "thsp=";
  cmd += s.screen_timeout;
  nex.sendCommand(cmd.c_str());
  // Preparing second core (Core 0) for UI tasks
  enableCore1WDT();
  xTaskCreatePinnedToCore(tft_task, "TFT", 10000, NULL, 10, &tftUpdates, 0);
}

void tft_task(void* param) {
  while (!tft_enable) {
    delay(100);
  }
  for (;;) {
    // The things below are needed to prevent crashes due to
    // watchdog errors
    TIMERG0.wdt_wprotect = TIMG_WDT_WKEY_VALUE;
    TIMERG0.wdt_feed = 1;
    TIMERG0.wdt_wprotect = 0;
    tft_loop();
    delay(10);
  }
}

void handleMessage(String& msg) {
  if (msg.startsWith("86")) {
    Serial.println("Screen sleeping");
    nex.sendCommand("wup=1");
    currPage = 1;
    isSleeping = true;
  } else if (msg.startsWith("87")) {
    Serial.println("Screen is awake");
    isSleeping = false;
    currPage = 1;
  } else if (msg.startsWith("65 1 6 1")) {
    Serial.println("Page 2");
    currPage = 2;
  } else if (msg.startsWith("65 2 5 1")) {
    Serial.println("Page 1");
    currPage = 1;
  } else if (msg.startsWith("65 2 2 1")) {
    Serial.println("Page 3");
    currPage = 3;
    populateSettingsPage();
  } else if (msg.startsWith("65 3 2 1")) {
    Serial.println("Page 2");
    currPage = 2;
  } else if (msg.startsWith("65 3 1 1")) {  // Config Save button pressed
    saveConfiguration();
  } else if (msg.startsWith("65 2 3 1")) {  // pH calibration page
    Serial.println("Page 4");
    currPage = 4;
    populatePhCalibrationPage();
  } else if (msg.startsWith("65 4 1 1")) {
    Serial.println("Page 2");
    currPage = 2;
  } else if (msg.startsWith("65 3 f 0")) {
    toggleConfigDateTime();
  } else if (msg.startsWith("65 4 9 0")) {  // Save calibration point #1
    saveCalibrationConfig(1);
  } else if (msg.startsWith("65 4 a 0")) {  // Save calibration point #2
    saveCalibrationConfig(2);
  }
}

int lastmillis = 0;
void tft_loop() {
  String msg = nex.listen();
  // if (msg.length() > 0)
  //   Serial.println(msg);
  handleMessage(msg);
  int currMills = millis();
  if (!isSleeping && (currMills - lastmillis > 1000)) {
    lastmillis = currMills;
    updateStats();
    updateDateTime();
    updatePhCalibrationPage();
  }
}