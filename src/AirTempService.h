#pragma once

#include <HttpEndpoint.h>
#include <dht11.h>

#define AIR_TEMP_ENDPOINT "/rest/airTemp"

class airTempState {
  public:
    static int airTemp;
    static int airHumidity;
    static String tstamp;

    static void read(airTempState& settings, JsonObject& root);
};

class AirTempService: public StatefulService<airTempState> {
  public:
    AirTempService(AsyncWebServer* server, SecurityManager* securityManager, int gpio);
    ~AirTempService();
    void begin();
    void loop();
  private:
    dht11* sensor;
    HttpGetEndpoint<airTempState> _httpEndpoint;
};