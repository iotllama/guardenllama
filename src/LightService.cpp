#include <LightService.h>
#include <Arduino.h>

LightService::LightService(AsyncWebServer* server,
                          SecurityManager* securityManager):
  _httpEndpoint(Light::read,
                this,
                server,
                LIGHT_SETTINGS_ENDPOINT_PATH,
                securityManager,
                AuthenticationPredicates::NONE_REQUIRED)
{
  pinMode(LED_BUILTIN, OUTPUT);
}

void LightService::begin()
{
  _state.ledOn = LED_ON;
  onConfigUpdated();
}

void LightService::onConfigUpdated() {
  digitalWrite(LED_BUILTIN, _state.ledOn ? LED_ON : LED_OFF);
  //digitalWrite(LED_BUILTIN, HIGH);
}

void LightService::turnOn() {
  _state.ledOn = !LED_ON;
  onConfigUpdated();
}

void LightService::turnOff() {
  _state.ledOn = !LED_OFF;
  onConfigUpdated();
}


