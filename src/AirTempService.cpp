#include <AirTempService.h>
#include <helper_functions.h>

int airTempState::airTemp = -1;
int airTempState::airHumidity = -1;
String airTempState::tstamp = "NA";

void airTempState::read(airTempState& settings, JsonObject& root) {
  root["airtemp"] = String(airTemp);
  root["airhumidity"] = String(airHumidity);
  root["tstamp"] = getTimeStamp();
}

AirTempService::AirTempService(AsyncWebServer* server, SecurityManager* securityManager, int gpio) :
  _httpEndpoint(airTempState::read,
                this,
                server,
                AIR_TEMP_ENDPOINT,
                securityManager,
                AuthenticationPredicates::NONE_REQUIRED) {

  sensor = new dht11(gpio);
}
AirTempService::~AirTempService() {
  delete sensor;
}

void AirTempService::begin() {
  sensor->begin();
}

void AirTempService::loop() {
  int tmp = sensor->getTemperature();
  if(tmp < 100000)
    _state.airTemp = tmp;
  tmp = sensor->getHumidity();
  if(tmp < 100000)
    _state.airHumidity = tmp;
}