#pragma once

#include <HttpEndpoint.h>
#include <ph_probe.h>

#define PH_ENDPOINT "/rest/ph"
#define PH_CALIB_ENDPOINT "/rest/ph_calibration"

class pHState {
 public:
  static float pHValue;
  static float cal1ph, cal2ph;
  static float caltemp;
  static int calpt;
  static int adc;
  static String tstamp;

  static void readall(pHState& settings, JsonObject& root);
  static void readph(pHState& settings, JsonObject& root);
  static StateUpdateResult update(JsonObject& root, pHState& settings);
};

class pHService : public StatefulService<pHState> {
 public:
  pHService(AsyncWebServer* server, SecurityManager* securityManager, int addr);

 private:
  pHState _phState;
  HttpGetEndpoint<pHState> _httpEndpoint;
};

class pHCalibrationService : public StatefulService<pHState> {
 public:
  pHCalibrationService(AsyncWebServer* server, SecurityManager* securityManager, int addr);
  static void updateCalibrationParams(float cal1ph = 0, float cal2ph = 0, int cal1adc = 0, int cal2adc = 0, float caltemp = 0);
  void begin();
  void loop();

 private:
  static PH_Probe pHProbe;
  HttpEndpoint<pHState> _httpEndpoint;

  void updateConfig();
};