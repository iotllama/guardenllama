#include <Arduino.h>
#include <guarden_tft.h>
#include <ESP8266React.h>
#include <pHService.h>
#include <WaterTempService.h>
#include <AirTempService.h>
//#include <WiFiStatus.h>

#define ADDR 0x4D
#define WATER_TEMP_BUS 25
#define AIR_TEMP_GPIO 0
#define TFT_POWER_CTL_PIN GPIO_NUM_32

AsyncWebServer server(80);
ESP8266React esp8266React(&server);
pHService* phService;
pHCalibrationService* phCalibrationService;
WaterTempService waterTempService(&server, esp8266React.getSecurityManager(), WATER_TEMP_BUS);
AirTempService airTemp(&server, esp8266React.getSecurityManager(), AIR_TEMP_GPIO);
bool ledOn = false;

void setup() {
  pinMode(TFT_POWER_CTL_PIN, OUTPUT);
  digitalWrite(TFT_POWER_CTL_PIN, HIGH);
  delay(1000);
  Serial.begin(115200);
  Serial.println("Entering setup");
  setup_tft();
  read_settings();
  Wire.begin();
  pinMode(BUILTIN_LED, OUTPUT);
  digitalWrite(BUILTIN_LED, HIGH);
  phService = new pHService(&server, esp8266React.getSecurityManager(), ADDR);
  phCalibrationService = new pHCalibrationService(&server, esp8266React.getSecurityManager(), ADDR);
  if (s.wifi_on)
    esp8266React.begin();
  phCalibrationService->begin();
  waterTempService.begin();
  airTemp.begin();
  server.begin();
  Serial.println("Exit setup");
}

void blinkLed() {
  WiFiMode_t currentWiFiMode = WiFi.getMode();
  wl_status_t status = WiFi.status();
  if ((currentWiFiMode == WIFI_MODE_STA || currentWiFiMode == WIFI_MODE_APSTA) && (status == WL_CONNECTED)) {
    digitalWrite(BUILTIN_LED, LOW);
    ledOn = true;
  } else if ((currentWiFiMode == WIFI_MODE_AP) || (currentWiFiMode == WIFI_MODE_APSTA)) {
    digitalWrite(BUILTIN_LED, ledOn ? HIGH : LOW);
    ledOn = !ledOn;
  } else {
    digitalWrite(BUILTIN_LED, HIGH);
    ledOn = false;
  }
}

void runEverySecond() {
  waterTempService.updateWaterTemp();
  airTemp.loop();
  blinkLed();
}

unsigned long lastMills = 0;

void loop() {
  // tft_loop();
  if (s.wifi_on) {
    if (WiFi.getMode() != WIFI_OFF) {
      esp8266React.loop();
    } else {
      Serial.println("Restarting Wifi");
      esp8266React.begin();
      esp8266React.loop();
    }
  } else if (WiFi.getMode() != WIFI_OFF) {
    Serial.println("Disabling Wifi");
    WiFi.disconnect();
    WiFi.mode(WIFI_OFF);
    WiFi.setSleep(true);
  }
  phCalibrationService->loop();
  unsigned long mills = millis();
  if (mills - lastMills > 1000) {
    runEverySecond();
    lastMills = mills;
  }
  tft_enable = true;
}
