#include <WaterTempService.h>
#include <helper_functions.h>

float waterTempState::waterTemp = -1;
String waterTempState::tstamp = "NA";

void waterTempState::read(waterTempState& settings, JsonObject& root) {
  root["value"] = String(waterTemp, 1);
  root["tstamp"] = getTimeStamp();
}

WaterTempService::WaterTempService(AsyncWebServer* server, SecurityManager* securityManager, int gpio) :
    _httpEndpoint(waterTempState::read,
                  this,
                  server,
                  WATER_TEMP_ENDPOINT,
                  securityManager,
                  AuthenticationPredicates::NONE_REQUIRED) {
  OneWire* _onewire = new OneWire(gpio);
  tempProbe = new DS18B20(_onewire);
}
WaterTempService::~WaterTempService() {
  delete tempProbe;
}

void WaterTempService::begin() {
  tempProbe->begin();
}

void WaterTempService::updateWaterTemp() {
  tempProbe->setResolution(12);
  tempProbe->requestTemperatures();
  long startMillis = millis();
  while (!tempProbe->isConversionComplete()) {
    if((millis() - startMillis) > 750)
      return;
  }

  float tt = tempProbe->getTempC();
  if (tt > 1) {
    _waterTempState.waterTemp = tt;
  }
}